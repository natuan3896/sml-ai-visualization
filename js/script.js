var mapObj = null;
var defaultCoord = [10.7743, 106.6669]; // coord mặc định, 9 giữa HCMC
var zoomLevel = 13;
var mapConfig = {
  attributionControl: false, // để ko hiện watermark nữa
  center: defaultCoord, // vị trí map mặc định hiện tại
  zoom: zoomLevel, // level zoom
};
var itemColor = [];
var colors = [
  "#008080",
  "#C71585",
  "#800080",
  "#40E0D0",
  "#CC9933",
  "#7FFF00",
  "#00EEEE",
  "#0000CD",
  "#A0522D",
  "#6E8B3D",
  "#CD0000",
  "#CDAA7D",
  "#FFB90F",
  "#FFF68F",
  "#ADFF2F",
  "#4169E1",
];

var routeColor = [];
var routeDisplayIndex = null;

$(document).ready(function () {
  mapObj = L.map("mapVisualize", { attributionControl: false }).setView(
    defaultCoord,
    zoomLevel
  );

  // add tile để map có thể hoạt động, xài free từ OSM
  L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", {
    attribution:
      '© <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
  }).addTo(mapObj);

  // tạo marker
  var popupOption = {
    className: "map-popup-content",
  };
  // var marker = addMarker(
  //   [10.7743, 106.6669],
  //   `<div class='left'><img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1SGNN50inDZcOweium4llf4qacFBFgBK9sXW7fxQ_lBm6-Abcww' /></div><div class='right'><b>Đây có gì hot?</b><br>Một Popup có 1 cô gái tên là Lailah</div><div class='clearfix'></div>`,
  //   popupOption
  // );

  var holder = document.getElementById("holder"),
    state = document.getElementById("status");

  if (typeof window.FileReader === "undefined") {
    state.className = "fail";
  } else {
    state.className = "success";
    state.innerHTML = "File API & FileReader available";
  }

  holder.ondragover = function () {
    this.className = "hover";
    return false;
  };
  holder.ondragend = function () {
    this.className = "";
    return false;
  };
  holder.ondrop = function (e) {
    this.className = "";
    e.preventDefault();

    var file = e.dataTransfer.files[0],
      reader = new FileReader();
    // fileName = file.name.split("_")[0];

    reader.onload = function (event) {
      data = JSON.parse(event.target.result);
      // console.log(e);
      // console.log(fileName);
      dataProcessed = processDataFileOutput(data);
      createSolutionButton(dataProcessed.numberOfSolution);
      $("#option-wrap .checkbox").css("display", "block");
      $(".solution").on("click", function () {
        index = $(this).attr("data-solution");
        if ($(".solution").hasClass("atv-solution")) {
          $(".solution").removeClass("atv-solution");
        }
        $(this).addClass("atv-solution");
        // lấy data từ solution ra
        dataSolution = getDataFromSolution(data.solutions[index]);
        // init màu cho các route
        for (let i = 0; i < dataSolution.routes.length; i++) {
          if (routeColor.length <= dataSolution.routes.length) {
            routeColor.push(colors[i]);
          }
        }
        // console.log(dataSolution);
        $(".chart-wrapper").html("");
        $(".list-vehicle").html("");
        // clearMap();
        $("#option-show")[0].checked = false;
        clearMap();

        dataSolution.routes.forEach((element, i) => {
          // console.log(element.vehicle)

          // console.log(routeColor[i])

          addVehicleToListVehicleDOM(
            element.vehicle,
            i,
            routeColor[i],
            element.informartionOfDeli
          );

          $(".item" + i).on("click", function () {
            if ($(".vehicle-item").hasClass("active")) {
              $(".vehicle-item").removeClass("active");
            }
            $(this).addClass("active");
            // console.log(vehicleItem)
            routeDisplayIndex = i;
            $(".chart-wrapper").html("");

            createTimeChart(
              element.informartionOfDeli,
              element.infoOfVehicleAtDeli,
              window.innerWidth - 90,
              window.innerHeight,
              100,
              i,
              routeColor[i]
            );
            createUnaryDimensionChart(
              "weight",
              element.loads,
              window.innerHeight,
              window.innerWidth - 90,
              100
            );

            // window.mapObj.removeLayer(window.polygon);
            // console.log($("#option-show")[0].checked);
            if ($("#option-show")[0].checked == false) {
              clearMap();
              createRoutePolygon(element.deliLocations, routeColor[i]);
            }
            // console.log(mapObj);
          });
        });

        $("#option-show").change(function () {
          if (this.checked) {
            clearMap();
            dataSolution.routes.forEach((element, i) => {
              createRoutePolygon(element.deliLocations, routeColor[i]);
            });
          } else {
            clearMap();
            if (routeDisplayIndex !== null) {
              createRoutePolygon(
                dataSolution.routes[routeDisplayIndex].deliLocations,
                routeColor[routeDisplayIndex]
              );
            }
          }
        });
      });

      // var timeOC, timeAD, widthCanvasTimeChart, heightCanvasTimeChart;
    };

    reader.readAsText(file);

    return false;
  };
  // createTimeChart(timeOpenClose);
});

function createRoutePolygon(deliLocations, color) {
  deliLocations.forEach((item, index) => {
    mapObj.addLayer(
      new L.Marker(item, {
        icon: new L.DivIcon({
          className: "lable-map",
          html: `<span style="background-color:${color};"> ${index}</span>`,
        }),
      })
    );
  });

  var polygon = L.polygon(deliLocations, {
    color: color,
  }).addTo(mapObj);
}

function clearMap() {
  for (i in mapObj._layers) {
    if (mapObj._layers[i]._path != undefined) {
      try {
        mapObj.removeLayer(mapObj._layers[i]);
      } catch (e) {
        console.log("problem with " + e + mapObj._layers[i]);
      }
    }
  }
  $(".leaflet-marker-pane").html("");
}
function addMarker(coord, popupContent, popupOptionObj, markerObj) {
  if (!popupOptionObj) {
    popupOptionObj = {};
  }
  if (!markerObj) {
    markerObj = {};
  }

  var marker = L.marker(coord, markerObj).addTo(mapObj); // chơi liều @@
  var popup = L.popup(popupOptionObj);
  popup.setContent(popupContent);

  // binding
  marker.bindPopup(popup);

  return marker;
}

function processDataFileOutput(data) {
  var obj = {
    numberOfSolution: 0,
  };
  //
  // console.log(fileName);
  obj.numberOfSolution = data.solutions.length;

  return obj;
}

function getDataFromSolution(solution) {
  // console.log(solution);
  var objSolution = {};
  var routes = [];
  solution.routes.forEach((route, index) => {
    objRoute = {};
    informartionOfDeli = [];
    infoOfVehicleAtDeli = [];
    loads = [];
    deliLocations = [];
    for (
      let i = route.elements.length / 2;
      i < route.elements.length - 1;
      i++
    ) {
      const element = route.elements[i];

      timeOpen = convertTimeFormatToSecond(element.earlyDeliveryTime);
      timeClose = convertTimeFormatToSecond(element.lateDeliveryTime);
      timeArrival = convertTimeFormatToSecond(element.arrivalTime);
      timeDeparture = convertTimeFormatToSecond(element.departureTime);

      // console.log(timeOpen);

      tempObjData = {
        description: element.description,
      };
      tempObjData = JSON.stringify(tempObjData);

      obj2 = {
        arrival: timeArrival,
        departure: timeDeparture,
      };
      loads.push(element.load);
      items = element.items;
      // nbOfEachTypeProduct = { dry: 0, cold: 0, ice: 0 };

      // nbOfType.set("a", "1234");
      // nbOfType.set("b", "1234");

      // console.log(nbOfType);
      nbOfEachTypeProduct = items.reduce((nbOfType, item, index, items) => {
        if (nbOfType.has(item.itemType)) {
          nbOfType.set(item.itemType, nbOfType.get(item.itemType) + 1);
        } else {
          nbOfType.set(item.itemType, 1);
          if (!(item.itemType in itemColor)) {
            itemColor[item.itemType] = getRandomColor();
          }
        }
        return nbOfType;
      }, new Map());
      var mapSorted = new Map([...nbOfEachTypeProduct].sort());
      // console.log(nbOfEachTypeProduct.get(nbOfEachTypeProduct.get(a)));

      obj1 = {
        open: timeOpen,
        close: timeClose,
        data: tempObjData,
        nbOfEachTypeProduct: mapSorted,
      };

      deliLocations.push([element.lat, element.lng]);
      informartionOfDeli.push(obj1);
      infoOfVehicleAtDeli.push(obj2);
    }

    // console.log(route.elements[0].lat + " ===== " + route.elements[0].lng);
    deliLocations.unshift([route.elements[0].lat, route.elements[0].lng]);
    objRoute.informartionOfDeli = informartionOfDeli;
    objRoute.infoOfVehicleAtDeli = infoOfVehicleAtDeli;
    objRoute.vehicle = route.vehicle;
    objRoute.loads = loads;
    objRoute.deliLocations = deliLocations;
    routes[index] = objRoute;
    // console.log(routes[index]);
  });

  objSolution.routes = routes;
  return objSolution;
}

function createSolutionButton(solutions) {
  htmlString = "";
  for (let i = 0; i < solutions; i++) {
    htmlString += `
            <button type="button" class="btn btn-info solution " data-solution="${i}">
                Solution ${i}
            </button>
        `;
  }
  $(".list-solutions").html(htmlString);
}

function addVehicleToListVehicleDOM(vehicle, i, color, inforDeli) {
  // console.log(inforDeli);
  domType = "";
  // console.log(inforDeli);
  inforDeli.forEach((item) => {
    domType += "<li>";

    for (let [key, value] of item.nbOfEachTypeProduct) {
      domType += `<span style="background-color: ${itemColor[key]}">${
        value + " " + key
      }</span>`;
    }

    domType += "</li>";
  });

  // console.log(domType);
  htmlString = `
    <div class="vehicle-item item${i}" style="background-color: ${color};" data-index="${i}" >
      <div class="vi-name">
          <p>${vehicle.code}</p>
      </div>
      <ul class="vi-content">
        ${domType}
      </ul>
    </div>
  `;

  $(".list-vehicle").css("padding", "1px 10px");
  $(".list-vehicle").append(htmlString);
}

// ==============helper============
function getRandomColor() {
  var letters = "123456789ABCDE";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 14)];
  }
  return color;
}
function convertTimeFormatToSecond(timeFormat) {
  reg = /\d\d:\d\d:\d\d/;
  huorsElement = timeFormat.match(reg)[0].split(":", 3);

  timeSecond =
    Number(huorsElement[0]) * 3600 +
    Number(huorsElement[1]) * 60 +
    Number(huorsElement[2]);
  return timeSecond;
}

function convertTimeSecondToFormat(timeSecond) {
  hour = Math.floor(timeSecond / 3600) + "";
  hour.length == 1 ? (hour = "0" + hour) : (hour = hour);
  minute = Math.floor((timeSecond - hour * 3600) / 60) + "";
  minute.length == 1 ? (minute = "0" + minute) : (minute = minute);
  second = timeSecond - hour * 3600 - minute * 60 + "";
  second.length == 1 ? (second = "0" + second) : (second = second) + "";
  format = "" + hour + ":" + minute + ":" + second;

  return format;
}

function createTimeChart(
  timeOC,
  timeArrivalDeparture,
  canvasWidth,
  canvasHeight,
  padding,
  index,
  routeColor
) {
  var numberOfDelivery = timeOC.length;

  $(".chart-wrapper").append(
    `
    <div class="chart-time chart-item" >
      <canvas id='chart-time${index}' width='${canvasWidth}' height='${canvasHeight}'> </canvas>
      <div class="chart-title"> 
        <p> Time of Route ${index}</p> 
      </div>
    </div>
    `
  );

  canvas = new fabric.Canvas(`chart-time${index}`);
  // fabric.Object.prototype.transparentCorners = true;
  // =============================================
  // ============ Event ==========================
  canvas.on("mouse:over", function (e) {
    // console.log(e.target.type);
    if (e.target !== null) {
      if (e.target.type == "rect" || e.target.type == "line") {
        // console.log(e.e.clientX);
        $("body").append(`
        <div id="detail-content" style="  top: ${e.e.clientY - 200}px; left: ${
          e.e.clientX
        }px; ">
          <p> ${e.target.data} </p>
        </div>
      `);
      }
    }

    // canvas.renderAll();
  });

  canvas.on("mouse:out", function (e) {
    $("#detail-content").remove();
  });
  // =============================================
  // =============================================
  var minOpen = 999999999,
    maxClose = 0;
  outstandingPoints = new Set();

  timeOC.forEach((item) => {
    // item.open = convertTimeFormatToSecond(item.open);
    minOpen = Math.min(minOpen, item.open);
    // item.close = convertTimeFormatToSecond(item.close);
    maxClose = Math.max(item.close, maxClose);
    outstandingPoints.add(item.close).add(item.open);
  });
  outstandingPoints = Array.from(outstandingPoints);
  outstandingPoints.sort(function (a, b) {
    return a - b;
  });
  // console.log(outstandingPoints);
  oPoint = reverseOY(canvasHeight, { x: padding, y: padding });
  OxExtremePoints = reverseOY(canvasHeight, {
    x: canvasWidth - padding,
    y: padding,
  });
  OyExtremePoints = reverseOY(canvasHeight, {
    x: padding,
    y: canvasHeight - padding,
  });

  oyAxis = drawLine(
    [oPoint.x, oPoint.y, OxExtremePoints.x, OxExtremePoints.y],
    "#333"
  );
  oxAxis = drawLine(
    [oPoint.x, oPoint.y, OyExtremePoints.x, OyExtremePoints.y],
    "#333"
  );
  canvas.add(oyAxis, oxAxis);
  valueAreaX = canvasWidth - padding * 2;
  valueAreaY = canvasHeight - padding * 2;

  baseUnit = (maxClose - minOpen) / valueAreaY;
  pathString = "M " + oPoint.x + " " + oPoint.y;

  timeOC.forEach((item, index) => {
    console.log(item);
    var heightRect = Math.round((item.close - item.open) / baseUnit);
    var widthRect = 20;
    var left =
      Math.ceil(valueAreaX * ((index + 1) / numberOfDelivery)) + padding;
    var top = canvasHeight - padding - (item.open - minOpen) / baseUnit;

    arrPoint = reverseOY(canvasHeight, {
      y: Math.ceil(
        (timeArrivalDeparture[index].arrival - minOpen) / baseUnit + padding
      ),
      x: left - widthRect / 2,
    });
    console.log(timeArrivalDeparture[index]);
    departPoint = reverseOY(canvasHeight, {
      y: Math.ceil(
        (timeArrivalDeparture[index].departure - minOpen) / baseUnit + padding
      ),
      x: left - widthRect / 2,
    });

    if (index > 0) {
      prevDepartPoint = reverseOY(canvasHeight, {
        y: Math.ceil(
          (timeArrivalDeparture[index - 1].departure - minOpen) / baseUnit +
            padding
        ),
        x:
          Math.ceil(valueAreaX * (index / numberOfDelivery)) +
          padding -
          widthRect / 2,
      });
    } else {
      prevDepartPoint = { ...oPoint };
    }

    dataDeli = JSON.parse(item.data);

    // Write Index Ox
    // displayIndex = ;
    // console.log(displayIndex);
    indexContent = writeIndexContent(
      index + 1 + "",
      left - widthRect,
      canvasHeight - padding * 0.8
    );
    indexContent.selectable = false;
    canvas.add(indexContent);

    // ===========Create new lineChart by Line===========

    lineAtElement = drawLine(
      [arrPoint.x, arrPoint.y, departPoint.x, departPoint.y],
      routeColor,
      ""
    );
    lineToPrevElement = drawLine(
      // cộng 1 vào arrPoint để cho nó cua đỡ gắt
      [prevDepartPoint.x, prevDepartPoint.y, arrPoint.x + 1, arrPoint.y],
      routeColor,
      "timeToNextElement : "
      // phần này đang sai, đây chỉ là làm mẫu để demo
      // (timeArrivalDeparture[index + 1].arrival -
      //   timeArrivalDeparture[index].departure)
    );

    // ==================================================

    rect = drawVerticalRectangle(
      widthRect,
      heightRect,
      left,
      top,
      dataDeli.description
    );
    rect.selectable = false;

    canvas.add(rect);
    canvas.add(lineAtElement);
    canvas.add(lineToPrevElement);
  });

  outstandingPoints.forEach((item) => {
    point = reverseOY(canvasHeight, {
      x: 0,
      y: Math.floor((item - minOpen) / baseUnit + padding),
    });

    content = convertTimeSecondToFormat(item);
    indexContent = writeIndexContent(content, point.x, point.y - 10);
    canvas.add(indexContent);
  });
}

function createUnaryDimensionChart(
  dimensionName,
  dimensionsArray,
  canvasHeight,
  canvasWidth,
  padding
) {
  $(".chart-wrapper").append(`
    <div class="chart-item" >
      <canvas id="chart-${dimensionName}" width="${canvasWidth}" height="${canvasHeight}"> </canvas>
      <div class="chart-title"> 
        <p> Weight </p> 
      </div>
    </div>
    
  `);
  // weights = [10000, 8800, 7777, 5320, 2225];

  var nbDeli = dimensionsArray.length;
  canvas = new fabric.Canvas(`chart-weight`);
  oPoint = reverseOY(canvasHeight, { x: padding, y: padding });
  OxExtremePoints = reverseOY(canvasHeight, {
    x: canvasWidth - padding,
    y: padding,
  });
  OyExtremePoints = reverseOY(canvasHeight, {
    x: padding,
    y: canvasHeight - padding,
  });

  oyAxis = drawLine(
    [oPoint.x, oPoint.y, OxExtremePoints.x, OxExtremePoints.y],
    "#333"
  );
  oxAxis = drawLine(
    [oPoint.x, oPoint.y, OyExtremePoints.x, OyExtremePoints.y],
    "#333"
  );
  canvas.add(oyAxis, oxAxis);

  // vùng đã căn lề, chỉ vẽ trong này
  valueAreaX = canvasWidth - padding * 2;
  valueAreaY = canvasHeight - padding * 2;
  // phần tử lớn nhất trong mảng dimension
  maxInDimensionsArray = Math.max.apply(Math, dimensionsArray);
  // giá trị chiều cao của Oy cho một giá trị cơ sở của dimension
  baseUnit = maxInDimensionsArray / valueAreaY;

  dimensionsArray.forEach((item, index) => {
    heightRect = Math.round(item / baseUnit);
    // console.log(heightRect);
    widthRect = 20;
    leftRect = Math.ceil(valueAreaX * ((index + 1) / nbDeli)) + padding;
    topRect = canvasHeight - padding;

    rect = drawVerticalRectangle(
      widthRect,
      heightRect,
      leftRect,
      topRect,
      "dataDeli.description"
    );
    rect.selectable = false;

    note = writeIndexContent(
      item + "",
      leftRect,
      padding + valueAreaY - heightRect,
      "center",
      "bottom",
      16
    );

    // Write Index Ox
    indexContent = writeIndexContent(
      index + 1 + "",
      leftRect - widthRect,
      canvasHeight - padding * 0.8
    );
    indexContent.selectable = false;
    canvas.add(indexContent);

    canvas.add(rect);
    canvas.add(note);
  });

  // console.log(canvas);
}

function drawLine(coords, color, data) {
  return new fabric.Line(coords, {
    // fill: color,
    stroke: color,
    strokeWidth: 3,

    // padding: 5,
    data: data,
    selectable: false,
    evented: true,
  });
}

function writeIndexContent(
  content,
  x,
  y,
  originX = "left",
  originY = "center",
  fontSize = 20
) {
  return new fabric.Textbox(content, {
    left: x,
    top: y,
    fill: "#333",
    selectable: false,
    originX: originX,
    originY: originY,
    fontSize: fontSize,
    fontWeight: "500",
    // strokeWidth: 2,
    // stroke: "#D81B60",
  });
}

function drawLineChart(pathString) {
  //   pathString = "M 0 0 L 50 0 M 0 0 L 4 -3 M 0 0 L 4 3 z";

  return new fabric.Path(pathString, {
    // left: 100,
    // top: 100,
    padding: 0,
    stroke: "blue",
    strokeWidth: 1,
    fill: false,
  });
}

function drawVerticalRectangle(width, height, left, top, data) {
  //   console.log(top);
  return new fabric.Rect({
    left: left,
    top: top,
    originX: "right",
    originY: "bottom",
    width: width,
    height: height,
    data: data,
    fill: "rgba(132,112,255,0.5)",
    transparentCorners: false,
  });
}

function reverseOY(yMax, obj) {
  var temp = yMax - obj.y;
  return { x: obj.x, y: temp };
}
